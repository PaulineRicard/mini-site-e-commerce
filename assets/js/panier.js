console.log('panier.js');

$().ready(function () {

	$.ajax({
		url: 'http://stripe.bwb/products/articles',
		method: 'GET',
		success: function (result) {
			//alert(result);
			if (result == 'panier vide') {
				$('#quantity').hide();
			} else {
				$('#quantity').text(result);
				$('#button-panier').click(function () {
					$.ajax({
						url: 'http://stripe.bwb/products/getarticles',
						method: 'GET',
						success: function (result) {
							$('#body-articles').empty();
							console.log(result);
							$('#modalPanier').modal('show');
							//$('#contenu').text(result);
							let panier = JSON.parse(result);
							let codeProducts = Object.keys(panier);
							console.log(panier);
							console.log(panier[codeProducts[0]][0]);
							let totals = 0;
							for (let i = 0; i < codeProducts.length; i++) {
								let prixU = panier[codeProducts[i]][1];
								let qu = panier[codeProducts[i]][2]
								let totalArticle = (parseFloat(prixU) * parseFloat(qu)).toFixed(2);
								totals = totals + parseFloat(totalArticle);
                                let code = codeProducts[i];
                                if(parseInt(qu) > 0){
                                    $('#body-articles').append(
                                        '<tr>' +
                                        // `<th scope="row">${i}</th>` +
                                        `<th>${panier[codeProducts[i]][0]}</th>` +
                                        `<th scope="col">${code}</th>` +
                                        `<th scope="col"><button class='btn btn-outline-primary' type="button" id="del${i}">-</button>${qu}<button class='btn btn-outline-primary' type="button" id="add${i}">+</button></th>` +
                                        `<th scope="col">${prixU}</th>` +
                                        `<th id="totalrow${i}" scope="col">${totalArticle}</th>` +
                                        `<th ><button class='btn btn-outline-danger' type="button" id="deleterow${i}">delete</button></th>` +
                                        '</tr>');
                                }
								
								$(`#add${i}`).click(function () {
									//alert('click')
									$.ajax({
										url: `http://stripe.bwb/products/panier/${code}`,
										method: 'GET',
										success: function (res) {
                                            console.log(res);
                                           // location.reload();
                                            $('#button-panier').trigger( "click" );
										}
									})
								});
								$(`#del${i}`).click(function () {
									//alert('click')
									$.ajax({
										url: `http://stripe.bwb/products/del/${code}`,
										method: 'GET',
										success: function (res) {
                                            console.log(res);
                                           //location.reload();
                                            $('#button-panier').trigger( "click" );
										}
									})
								});
								$(`#deleterow${i}`).click(function () {
									//alert('click')
									$.ajax({
										url: `http://stripe.bwb/products/remove/${code}`,
										method: 'GET',
										success: function (res) {
                                            console.log(res);
                                            //location.reload();
                                            $('#button-panier').trigger( "click" );
										}
									})
								});
							}
							
							$('#totals').text(totals);
							$('#button-commander').click(function(){
								$.ajax({
									url: 'http://stripe.bwb/commande',
									method: 'POST',
									data: {'total' : totals},
									success: function (res) {
										alert(res);
										$('#quantity').empty();
										location.replace('/paiement');
									}
								})
							})
						}
					});

				});
			}
		}


	});

	// suppr: <i class="material-icons">add</i>
	// + : <i class="fas fa-plus"></i>
	// - : <i class="fas fa-minus"></i>

	$('#button-reset').click(function () {
		$.ajax({
			url: 'http://stripe.bwb/products/resetcart',
			method: 'GET',
			success: function () {
				location.reload();
			}
		})
    })
   
})
