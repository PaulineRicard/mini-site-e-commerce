<?php 
class OrderDetails extends CI_Model
{
    public function create($data)
    {
        $this->db->insert('orderdetails', $data);
    }

    public function retrieveByOrder($orderNumber)
    {
        $query = $this->db->get_where('orderdetails', array('orderNumber' => $orderNumber));
        return $query->result();
    }
}