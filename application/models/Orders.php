<?php 
class Orders extends CI_Model
{
    public function create($data)
    {
        $this->db->insert('orders', $data);
        return $this->db->insert_id();
    }


    public function retrieveByCustomerNumber($number)
    {
        $query = $this->db->get_where('orders', array('customerNumber' => $number));
        return $query->result();

    }
}