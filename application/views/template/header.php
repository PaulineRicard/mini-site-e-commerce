<?php ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <title>Document</title>

</head>

<body>
  <header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-success">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarColor01">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="/home">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="products">Products</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/history">Commandes</a>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link" href="#">About</a>
          </li> -->
          <li class="nav-item">
            <a class="nav-link" href="/logout">Logout</a>
          </li>
        </ul>
      </div>
      <button id="button-panier" type="button" class="btn btn-outline-secondary">Panier <span id="quantity" class="badge badge-light"></span>
    </nav>

    <div class="row">
      <div class="col-md-3 col-sm-6">
        <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
          <div id="modalPanier" class="modal" id="infos">
            <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Panier</h4>
                  <button type="button" class="close" data-dismiss="modal">
                    <span>&times;</span>
                  </button>
                </div>
                <div id='contenu' class="modal-body">
                  <table class="table">
                    <thead>
                      <tr>
                        <!-- <th scope="col">#</th> -->
                        <th scope="col">Article</th>
                        <th scope="col">Code Produit</th>
                        <th scope="col">Quantité</th>
                        <th scope="col">Prix unitaire</th>
                        <th scope="col">Total</th>
                      </tr>
                    </thead>
                    <tbody id="body-articles">
                    </tbody>
                    <tfoot class="alert alert-success">
                    <th scope="row">Totals</th>
                    <td id="totals"></td>
                    </tfoot>
                  </table> 
                </div>
                <div class="modal-footer">
                  <button id="button-commander" type="button" class="btn btn-success" data-dismiss="modal">Commander</button>
                  <button type="button" class="btn btn-primary" data-dismiss="modal">Fermer</button>
                  <button id="button-reset" type="button" class="btn btn-warning" data-dismiss="modal">Vider le panier</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="messageflash">
      <?php if(isset($_SESSION['messageSuccess'])){ ?>
<div class="alert alert-success"><?= $_SESSION['messageSuccess'] ?> </div>
    <?php  }elseif(isset($_SESSION['messageDanger'])){ ?>
      <div class="alert alert-danger"><?= $_SESSION['messageDanger'] ?> </div>
   <?php } elseif(isset($_SESSION['message'])){ ?>
    <div class="alert alert-warning"><?= $_SESSION['message'] ?> </div>
   <?php } ?>
     </div>
  </header>