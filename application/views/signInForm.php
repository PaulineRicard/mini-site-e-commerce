<div class="container col-lg-4 mt-4">
    <fieldset>
    <legend>Sign In</legend>
<?php 
echo form_open('signin'); ?>
 <div class="form-group">
 <label for="email">Email address</label>
<?php echo form_error('email'); //* correspond au name de l'input
echo form_input(array(
    'type'  => 'email',
    'name'  => 'email',
    'id'    => 'email',
    'value' => 'dmurphy@classicmodelcars.com',
    'class' => 'form-control'
)); ?>
</div>
<div class="form-group">
<label for="password">Password</label>
<?php echo form_error('password');
echo form_input(array(
    'type'  => 'password',
    'name'  => 'password',
    'id'    => 'password',
    'class' => 'form-control'
)); ?>
</div>
<?php
echo form_submit('', 'Sign In', array('class' => 'btn btn-success'));
echo form_close();
?>
 </fieldset>
</div>
<!-- <div class="container col-lg-4 mt-4">
<form>
  <fieldset>
    <legend>Sign In</legend>
    <div class="form-group">
      <label for="Email">Email address</label>
      <input name="email" type="email" class="form-control" id="Email" aria-describedby="emailHelp" placeholder="Enter email">
      <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
    </div>
    <div class="form-group">
      <label for="password">Password</label>
      <input type="password" class="form-control" id="Password" name="password" placeholder="Password">
    </div>
    
    <button type="submit" class="btn btn-success">SignIn</button>
  </fieldset>
</form>
</div> -->
