<?php ?>


<div class="table">
    <table class="table table-responsive-sm table-hover ">
        <thead>
            <tr>
                <th>orderNumber</th>
                <th>Date</th>
                <th>status</th>
                <th>#</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($datas as $key => $value) {
                //print_r($value);
                ?>
                <tr>
                    <td><?= $value->orderNumber ?></td>
                    <td><?= $value->orderDate ?></td>
                    <td><?= $value->status ?></td>
                    <td><button id="<?= $value->orderNumber ?>" type="button" class="btn btn-warning">Détails</button>
                    
                </td>
                </tr>
            <?php } ?>

        </tbody>
    </table>
</div>


<!-- MODAL -->
<div class="row">
      <div class="col-md-3 col-sm-6">
        <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
          <div id="modalDetail" class="modal" id="infos">
            <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Panier</h4>
                  <button type="button" class="close" data-dismiss="modal">
                    <span>&times;</span>
                  </button>
                </div>
                <div id='contenu' class="modal-body">
                  <table class="table">
                    <thead>
                      <tr>
                        
                        <th scope="col">Article</th>
                        <th scope="col">Code Produit</th>
                        <th scope="col">Quantité</th>
                        <th scope="col">Prix unitaire</th>
                        <th scope="col">Total</th>
                      </tr>
                    </thead>
                    <tbody id="body-articles">
                    </tbody>
                    <tfoot class="alert alert-success">
                    <th scope="row">Totals</th>
                    <td id="totals"></td>
                    </tfoot>
                  </table> 
                </div>
                <div class="modal-footer">
                  <button id="button-commander" type="button" class="btn btn-success" data-dismiss="modal">Commander</button>
                  <button type="button" class="btn btn-primary" data-dismiss="modal">Fermer</button>
                  <button id="button-reset" type="button" class="btn btn-warning" data-dismiss="modal">Vider le panier</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


