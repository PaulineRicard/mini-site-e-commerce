<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('SecurityMiddleWare', null, 'securitytkn');
        $this->load->library('session'); $this->load->database();
		$this->load->helper('url');
        $this->load->library('grocery_CRUD');

    }
}