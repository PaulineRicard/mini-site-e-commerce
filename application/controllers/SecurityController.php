<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class SecurityController extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
       
    }
   
    //! voir pour utiliser le form_validation
    public function signIn()
    {
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('Employees');
        //$this->load->controller('TokenController');
        //? var_dump($this->input->post());
        if (empty($this->input->post())) {
            $this->load->view('template/header');
            $this->load->view('signInForm');
            $this->load->view('template/footer');
        } else {
            $user = (new Employees())->login($this->input->post());
            if ($user === null) {
                $this->session->set_flashdata('messageDanger', 'Identifiants incorrects');
                redirect('/signin');
            } else {
                $_SESSION['user']['user'] = $user;
                $this->securitytkn->generateToken($user);
                redirect('/home');
            }
        }
    }

    public function getHome()
    {
        
        if ($this->securitytkn->acceptConnexion() == false) {
            $this->session->set_flashdata('messageDanger', 'Session expirée, veuillez vous reconnecter');
            redirect('/signin');

        } else {
            $this->load->view('template/header');
            $this->load->view('home');
            $this->load->view('template/footer');
        }
    }
    public function logOut()
    {
       
        $this->securitytkn->deactivate();
        session_destroy();
        redirect('/signin');
    }
}
