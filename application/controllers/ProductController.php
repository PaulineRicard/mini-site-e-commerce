<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ProductController extends MY_Controller
{

    //* Permet d'afficher la liste des produits et autres vues de GroceryCrud
    public function productsOutput($output = null)
	{
       $this->load->view('template/header');
       $this->load->view('listeProducts', (array)$output);
       $this->load->view('template/footer');
    }
    
    //* Methode Grocery Crud pour gérer l'affichage de la table products
    public function products()
	{ //* verification du token si la verif' n'est pas bonne, redirection vers la page de login + message d'erreur
        if ($this->securitytkn->acceptConnexion() == false) {
            $this->session->set_flashdata('messageDanger', 'Session expirée, veuillez vous reconnecter');
            redirect('/signin');
        //* si le token est verifié
        }else{
            try{
                //* instancie un nouvel objet grocery_CRUD
                $crud = new grocery_CRUD();
                
                //* defini le thème pour l'affichage
                $crud->set_theme('bootstrap');
                //* defini la table de la bdd a utilisé
                $crud->set_table('products');
                //* choix des colonnes a afficher
                $crud->columns('productName','productDescription','buyPrice');
                
                //* permet d'enlever les bouton add/edit/delete/export/print
                $crud->unset_add();
                $crud->unset_edit();
                $crud->unset_delete();
                $crud->unset_export();
                $crud->unset_print();
                //* ajoute un bouton (nom, '', route, icone)
                $crud->add_action('Ajouter au panier', '', 'products/panier','fas fa-plus-circle');
    
                //* variable output -> ce qui sera afficher  +  appel de la méthode qui affiche la vue
                $output = $crud->render();
                $this->productsOutput($output);
                
            }catch(Exception $e){
                show_error($e->getMessage().' --- '.$e->getTraceAsString());
            }  
        }
        
    }
    
    //* Methode qui gère l'ajout des articles au panier, prend en parametre le code du produit
    public function addToCart($productCode)
    {
        //* verification du token si la verif' n'est pas bonne, redirection vers la page de login + message d'erreur
        if ($this->securitytkn->acceptConnexion() == false) {
            $this->session->set_flashdata('messageDanger', 'Session expirée, veuillez vous reconnecter');
            redirect('/signin');
        //* si le token est verifié
        }else{
            //* charge le model 'Products'
       $this->load->model('Products');
            //* recupère toutes les infos sur le produit
       $product = (new Products())->getByCode($productCode);
       
       //* si il n'y a pas de variable de session pour le produit, on la défini
        if(!isset($_SESSION['user']['panier'][$productCode])){
            $_SESSION['user']['panier'][$productCode] = array($product->productName, $product->buyPrice, 1 );
        }else{
            //* sinon on augmente la quantité (+1)
            $quantity = $_SESSION['user']['panier'][$productCode][2];
            $_SESSION['user']['panier'][$productCode] = array($product->productName, $product->buyPrice, $quantity+1 );
        }
        //* message "ajout ok" + redirection vers liste produits
        $this->session->set_flashdata('messageSuccess', 'Ajouter au panier');
        redirect('/products');
    }
}
    //* Methode articles : calcul la quantité d'articles dans le panier
    public function articles()
    {
        $nbArticles = 0;
        //* si le panier existe (au moins 1 article)
        if(isset($_SESSION['user']['panier'])){
            $panier = $_SESSION['user']['panier'];
            //* boucle sur tous les produits du panier pour récuperer la quantité
            foreach($panier as $key => $value){
                //*var_dump($value);
                $nbArticles = $nbArticles + $value[2];
            }
            echo $nbArticles;
        }else{
            echo 'panier vide';
        }
    }

    //* retourne en json le contenu de la variable de session panier
    public function getArticles()
    {
        echo json_encode($_SESSION['user']['panier']);
    }


    //* remet a 0 le panier
    public function resetCart()
    {
        if ($this->securitytkn->acceptConnexion() == false) {
            $this->session->set_flashdata('messageDanger', 'Session expirée, veuillez vous reconnecter');
            redirect('/signin');

        }else{
            //* detruit la variable de session panier
        unset($_SESSION['user']['panier']);
        //* averti l'user
        $this->session->set_flashdata('message', 'Votre panier a été vidé !!');
        echo('success');
        }
    }

    //* diminue la quantité pour un article (-1)
    public function deleteOne($codeProduct)
    {
        $_SESSION['user']['panier'][$codeProduct][2] = $_SESSION['user']['panier'][$codeProduct][2] - 1;
        echo($_SESSION['user']['panier'][$codeProduct][2]);
    }

    //* supprime un article du panier (peu importe la quantité)
    public function deleteProduct($codeProduct)
    {
        unset( $_SESSION['user']['panier'][$codeProduct]);
        echo('retiré du panier');
    }
}