<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class RegisterController extends MY_Controller
{
    public function __construct()
    {
       parent::__construct();
       $this->load->model('Orders');
       $this->load->model('OrderDetails');
    }

    //* Enregistre le contenu du panier en bdd 
    public function registerCart()
    {
        //* si le token n'est pas vérifié redirige vers la page de login
        if ($this->securitytkn->acceptConnexion() == false) {
            $this->session->set_flashdata('messageDanger', 'Session expirée, veuillez vous reconnecter');
            redirect('/signin');

        }else{
        //* création du tableau pour le "create" de la table Orders
        $data = array(
            'orderDate' => $this->date(),
            'requiredDate' => $this->date(),
            'status' => 'In Process',
            'customerNumber' => $_SESSION['user']['user']->employeeNumber
        );
        //* la methode create retourne la dernière id inscrite dans la table après avoir ajouter la ligne
        $orderNumber = (new Orders())->create($data);
        $i = 1;
        //* pour chaque produit du panier -> ligne dans la table OrderDetails
        foreach($_SESSION['user']['panier'] as $key => $value){
            $data2 = array(
                'orderNumber' => $orderNumber,
                'productCode' => $key,
                'quantityOrdered' => $value[2],
                'priceEach' => $value[1],
                'orderLineNumber' => $i
            );
           (new OrderDetails())->create($data2);
            $i++;
        }
        //* vide la panier
        unset($_SESSION['user']['panier']);
        //* variable de session -> prix total de la commande
        $totalPrice = $this->input->post()['total'];
        $_SESSION['user']['total'] = floatval($totalPrice);
    }
    }

    //* retourne la date au moment ou la methode est appelée
    public function date()
    {
        $this->load->helper('date');
        date_default_timezone_set('Europe/Paris'); 
        return date('Y-m-d H:i:s');
    }

    //* affiche la page de paiement
    public function getPaiement()
    {
        if ($this->securitytkn->acceptConnexion() == false) {
            $this->session->set_flashdata('messageDanger', 'Session expirée, veuillez vous reconnecter');
            redirect('/signin');

        }else{
            $this->load->view('template/header');
            $this->load->view('paiement');
            $this->load->view('template/footer');
        }
    }


    //! paiement 
    //todo doc!! voir comment ca marche!!!
    public function stripePost()
    {
        require_once('application/libraries/stripe-php/init.php');
    
        \Stripe\Stripe::setApiKey($this->config->item('stripe_secret'));
     
        \Stripe\Charge::create ([
                "amount" => $_SESSION['user']['total'] * 100,//!montant
                "currency" => "usd",
                "source" => $this->input->post('stripeToken'),
                "description" => "Test payment from stripe.bwb" 
        ]);
            
        $this->session->set_flashdata('messageSuccess', 'Payment made successfully.');
        unset($_SESSION['user']['total']); 
        redirect('/home');
    }

    public function history()
    {
        if ($this->securitytkn->acceptConnexion() == false) {
            $this->session->set_flashdata('messageDanger', 'Session expirée, veuillez vous reconnecter');
            redirect('/signin');
        //* si le token est verifié
        }else{
            $history = (new Orders())->retrieveByCustomerNumber($_SESSION['user']['user']->employeeNumber);
            $datas['datas'] =  $history;
            $this->load->view('template/header');
            $this->load->view('history', $datas);
            $this->load->view('template/footer');
        }
    }

    public function orderDetail($id)
    {
        $datas = (new OrderDetails())->retrieveByOrder($id);
        echo json_encode($datas);
    }
}


